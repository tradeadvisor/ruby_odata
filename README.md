# ruby_odata

The **Open Data Protocol** (OData) is a fantastic way to query and update data over standard Web technologies.  The ruby_odata library acts as a consumer of OData services.

Fork of [https://github.com/visoft/ruby_odata](https://github.com/visoft/ruby_odata[)